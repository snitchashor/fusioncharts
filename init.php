<?php

$modInfo['fusioncharts']['name'] = "Fusioncharts 4.2";
$modInfo['fusioncharts']['abstract'] = "Displays a bargraph for monthly/weekly kills and losses.";
$modInfo['fusioncharts']['about'] = "Version 4.2 by Snitch Ashor, original mod by Xeltor.";

$nochart = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'EVE-IGB');

if (!$nochart)
{
	if(Config::get('fusioncharts_front_page')) {
		event::register('home_assembling', 'addfusioncharts::addHome');
	}
	if(Config::get('fusioncharts_alliance_page')) {
		event::register('allianceDetail_assembling', 'addfusioncharts::addAllianceDetail');
	}
	if(Config::get('fusioncharts_corp_page')) {
		event::register('corpDetail_assembling', 'addfusioncharts::addCorpDetail');
	}
	if(Config::get('fusioncharts_pilot_page')) {
		event::register('pilotDetail_assembling', 'addfusioncharts::addPilotDetail');
	}
}
class AddFusionCharts
{
    static function addCorpDetail($home)
    {
        $home->addBehind("summaryTable", "AddFusionCharts::graphPages");
    }
    
    static function addPilotDetail($home)
    {
        $home->addBehind("summaryTable", "AddFusionCharts::graphPages");
    }
	
    static function addAllianceDetail($home)
    {
        $home->addBehind("summaryTable", "AddFusionCharts::graphPages");
    }
	
    static function addHome($home)
    {
        $home->addBehind("summaryTable", "AddFusionCharts::graphHome");
    }
    
    static function graphHome($home)
    {
		require_once('mods/fusioncharts/grafico.inc.php');
		$html.= VisualizzaGrafico(GeneraDatiGrafico($home->getWeek(), $home->getYear()));
        return $html;
    }
	
    static function graphPages($home)
    {
		require_once('mods/fusioncharts/grafico.inc.php');

		$html = VisualizzaGrafico(GeneraDatiGrafico_pages($home));
        return $html;
    }
}

?>
