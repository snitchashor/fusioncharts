<?php
/**
 * @return array kills (dati per il grafico)
 * @param integer year (four digits) 
 * @param integer week of the year (1..53)
 * @desc This function retrieves the data (kills and losses) in given interval, for the graph.
 */
 
function GeneraDatiGrafico($week, $year)
    {
	if(Config::get('show_monthly')) {
		$_y = (int)edkURI::getArg('y', 1);
		$_m = (int)edkURI::getArg('m', 2);
		if(!$_y && !$_m) {
			$today = getdate();
			$days = date("t");
		} else {
                        $t_giorno = $_y."-".$_m."-01";
			$days = date("t", strtotime($t_giorno));
		}
		for ($day=1; $day <= $days; $day++)
			{
			if(!$_y && !$_m) {
				$giorno = $today["year"]."-".$today["mon"]."-".$day;
			} else {
				$giorno = $_y."-".$_m."-".$day;
			}

			$kills[$day][1]=date("j", strtotime(($giorno)));
			
				if ( config::get('cfg_allianceid') || config::get('cfg_corpid') || config::get('cfg_pilotid') )
				{
					TrovaLoss($giorno, $giorno, $cnt, $valore);
					$kills[$day][2]=$cnt;
					$kills[$day][4]=$valore;

					TrovaKills($giorno, $giorno, $cnt, $valore);
					$kills[$day][3]=$cnt;
					$kills[$day][5]=$valore;
				} else {
					TrovaPublic($giorno, $giorno, $cnt, $valore);
					$kills[$day][2]=$cnt;
					$kills[$day][4]=$valore;
				}
			}
	} else {
		for ($day=0; $day < 7; $day++)
			{
			$giorno        = mktimefromcw($year, $week, $day);

			$kills[$day][1]=date("l", strtotime(($giorno)));

			if ( config::get('cfg_allianceid') || config::get('cfg_corpid') || config::get('cfg_pilotid') )
				{
					TrovaLoss($giorno, $giorno, $cnt, $valore);
					$kills[$day][2]=$cnt;
					$kills[$day][4]=$valore;

					TrovaKills($giorno, $giorno, $cnt, $valore);
					$kills[$day][3]=$cnt;
					$kills[$day][5]=$valore;
				} else {
					TrovaPublic($giorno, $giorno, $cnt, $valore);
					$kills[$day][2]=$cnt;
					$kills[$day][4]=$valore;
				}
			}
	}
    return $kills;
    }

function GeneraDatiGrafico_pages($dtl)
    {
	    
	$week = date('W');
	$year = date('Y');
	if(Config::get('show_monthly')) {
			$today = getdate();
			$days = date("t");
		for ($day=1; $day <= $days; $day++)
			{
			$giorno = $today["year"]."-".$today["mon"]."-".$day;

			$kills[$day][1]=date("j", strtotime(($giorno)));

			TrovaLoss($giorno, $giorno, $cnt, $valore, $dtl);
			$kills[$day][2]=$cnt;
			$kills[$day][4]=$valore;

			TrovaKills($giorno, $giorno, $cnt, $valore, $dtl);
			$kills[$day][3]=$cnt;
			$kills[$day][5]=$valore;
			}
	} else {
    	for ($day=0; $day < 7; $day++)
        {
	        $giorno        = mktimefromcw($year, $week, $day);
	        
	        $kills[$day][1]=date("l", strtotime(($giorno)));
	
	        TrovaLoss($giorno, $giorno, $cnt, $valore, $dtl);
	        $kills[$day][2]=$cnt;
	        $kills[$day][4]=$valore;
	
	        TrovaKills($giorno, $giorno, $cnt, $valore, $dtl);
	        $kills[$day][3]=$cnt;
	        $kills[$day][5]=$valore;
        }
	}
	
    return $kills;
    }
    
function TrovaKills($startDay, $endDay, &$cnt, &$valore, $dtl = 0)
    {
		$qry  =new DBQuery();
		if($dtl != 0) {
			$ALLIANCE_ID = $dtl->all_id;
			$CORP_ID = $dtl->crp_id;
			$PILOT_ID = $dtl->plt_id;
			if($ALLIANCE_ID) {
				$sql = "
									SELECT 
										COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
									FROM 
										kb3_kills 
									WHERE 
										(kll_all_id = " . $ALLIANCE_ID . ") AND
										(kll_timestamp >= '$startDay 00:00:00') AND
										(kll_timestamp <= '$endDay 23:59:59')";
			} else if($CORP_ID) {
				$sql = "
									SELECT 
										COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
									FROM 
										kb3_kills 
									WHERE 
										(kll_crp_id = " . $CORP_ID . ") AND
										(kll_timestamp >= '$startDay 00:00:00') AND
										(kll_timestamp <= '$endDay 23:59:59')";
			} else if($PILOT_ID) {
				$sql = "
									SELECT 
										COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
									FROM 
										kb3_kills 
									WHERE 
										(kll_victim_id = " . $PILOT_ID . ") AND
										(kll_timestamp >= '$startDay 00:00:00') AND
										(kll_timestamp <= '$endDay 23:59:59')";
			}
		} else {
			$ALLIANCE_ID = config::get('cfg_allianceid');
			$CORP_ID = config::get('cfg_corpid');
			$PILOT_ID = config::get('cfg_pilotid');
			$sql = "
					SELECT 
						COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
					FROM 
						kb3_kills 
					WHERE ";
			$sql .= "(";
			if(sizeof($ALLIANCE_ID)) {
				if(sizeof($ALLIANCE_ID)==1) {
					$sql .= "(kll_all_id = " . $ALLIANCE_ID[0] . ")";
				} else {
					for($i = 0; $i < (sizeof($ALLIANCE_ID)-1); ++$i)
					{
						$sql .= "(kll_all_id = " . $ALLIANCE_ID[$i] . ") OR";
					}
					$sql .= "(kll_all_id = " . $ALLIANCE_ID[$i] . ")";
				}
			}
			if(sizeof($CORP_ID)) {
				if(sizeof($ALLIANCE_ID)) $sql .= "OR";
				if(sizeof($CORP_ID)==1) {
					$sql .= "(kll_crp_id = " . $CORP_ID[0] . ")";
				} else {
					for($i = 0; $i < (sizeof($CORP_ID)-1); ++$i)
					{
						$sql .= "(kll_crp_id = " . $CORP_ID[$i] . ") OR";
					}
					$sql .= "(kll_crp_id = " . $CORP_ID[$i] . ")";
				}
			}
			if(sizeof($PILOT_ID)) {
				if(sizeof($CORP_ID) || sizeof($ALLIANCE_ID)) $sql .= "OR";
				if(sizeof($PILOT_ID)==1) {
					$sql .= "(kll_victim_id = " . $PILOT_ID[0] . ")";
				} else {
					for($i = 0; $i < (sizeof($PILOT_ID)-1); ++$i)
					{
						$sql .= "(kll_victim_id = " . $PILOT_ID[$i] . ") OR";
					}
					$sql .= "(kll_victim_id = " . $PILOT_ID[$i] . ")";
				}
			}
			$sql .= ") AND (kll_timestamp >= '$startDay 00:00:00') AND
										(kll_timestamp <= '$endDay 23:59:59')";
			
		}
		$qry->execute($sql)
			or die($qry->getErrorMsg());
		$row   =$qry->getRow();

		$cnt   =$row['cnt'];
		$valore=round( $row['valore'] / 1000000, 2 );
    }
	
function TrovaLoss($startDay, $endDay, &$cnt, &$valore, $dtl = 0)
    {
	
	$qry  =new DBQuery();
	if($dtl != 0) {
		$ALLIANCE_ID = $dtl->all_id;
		$CORP_ID = $dtl->crp_id;
		$PILOT_ID = $dtl->plt_id;
		if($ALLIANCE_ID) {
			$sql = "
								SELECT 
									COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
								FROM 
									kb3_kills
								LEFT JOIN
									kb3_inv_all ON kb3_kills.kll_id=kb3_inv_all.ina_kll_id
								WHERE 
									(kb3_inv_all.ina_all_id = " . $ALLIANCE_ID . ") AND
									(kb3_kills.kll_all_id != " . $ALLIANCE_ID . ") AND
									(kb3_kills.kll_timestamp >= '$startDay 00:00:00') AND
									(kb3_kills.kll_timestamp <= '$startDay 23:59:59')";
		} else if($CORP_ID) {
			$sql = "
								SELECT 
									COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
								FROM 
									kb3_kills
								LEFT JOIN
									kb3_inv_crp ON kb3_kills.kll_id=kb3_inv_crp.inc_kll_id
								WHERE 
									(kb3_inv_crp.inc_crp_id = " . $CORP_ID . ") AND
									(kb3_kills.kll_crp_id != " . $CORP_ID . ") AND
									(kb3_kills.kll_timestamp >= '$startDay 00:00:00') AND
									(kb3_kills.kll_timestamp <= '$startDay 23:59:59')";
		} else if($PILOT_ID) {
			$sql = "
								SELECT 
									COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
								FROM 
									kb3_kills
								LEFT JOIN
									kb3_inv_detail ON kb3_kills.kll_id=kb3_inv_detail.ind_kll_id
								WHERE 
									(kb3_inv_detail.ind_plt_id = " . $PILOT_ID . ") AND
									(kb3_kills.kll_victim_id != " . $PILOT_ID . ") AND
									(kb3_kills.kll_timestamp >= '$startDay 00:00:00') AND
									(kb3_kills.kll_timestamp <= '$endDay 23:59:59')";
		}
	} else {
		$ALLIANCE_ID = config::get('cfg_allianceid');
			$CORP_ID = config::get('cfg_corpid');
			$PILOT_ID = config::get('cfg_pilotid');
			if(sizeof($ALLIANCE_ID)>0) {
				$sql = "
					SELECT 
						COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
					FROM 
						kb3_kills
					LEFT JOIN
						kb3_inv_all ON kb3_kills.kll_id=kb3_inv_all.ina_kll_id
					WHERE (";
				if(sizeof($ALLIANCE_ID)<2) {
					$sql .= "((kb3_inv_all.ina_all_id = " . $ALLIANCE_ID[0] . ") AND (kb3_kills.kll_all_id != " . $ALLIANCE_ID[0] . "))";
				} else {
					for($i = 0; $i < (sizeof($ALLIANCE_ID)-1); ++$i)
					{
						$sql .= "((kb3_inv_all.ina_all_id = " . $ALLIANCE_ID[$i] . ") AND (kb3_kills.kll_all_id != " . $ALLIANCE_ID[$i] . ")) OR";
					}
					$sql .= "((kb3_inv_all.ina_all_id = " . $ALLIANCE_ID[$i] . ") AND (kb3_kills.kll_all_id != " . $ALLIANCE_ID[$i] . "))";
				}
			} else if(sizeof($CORP_ID)>0) {
				$sql = "
					SELECT 
						COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
					FROM 
						kb3_kills
					LEFT JOIN
						kb3_inv_crp ON kb3_kills.kll_id=kb3_inv_crp.inc_kll_id
					WHERE (";
				if(sizeof($CORP_ID)<2) {
					$sql .= "((kb3_inv_crp.inc_crp_id = " . $CORP_ID[0] . ") AND (kb3_kills.kll_crp_id != " . $CORP_ID[0] . "))";
				} else {
					for($i = 0; $i < (sizeof($CORP_ID)-1); ++$i)
					{
						$sql .= "((kb3_inv_crp.inc_crp_id = " . $CORP_ID[$i] . ") AND (kb3_kills.kll_crp_id != " . $CORP_ID[$i] . ")) OR";
					}
					$sql .= "((kb3_inv_crp.inc_crp_id = " . $CORP_ID[$i] . ") AND (kb3_kills.kll_crp_id != " . $CORP_ID[$i] . "))";
				}
			} else if(sizeof($PILOT_ID)>0) {
				$sql = "
					SELECT 
						COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
					FROM 
						kb3_kills
					LEFT JOIN
						kb3_inv_detail ON kb3_kills.kll_id=kb3_inv_detail.ind_kll_id
					WHERE (";
				if(sizeof($PILOT_ID)<2) {
					$sql .= "((kb3_inv_detail.ind_plt_id = " . $PILOT_ID[0] . ") AND (kb3_kills.kll_victim_id != " . $PILOT_ID[0] . "))";
				} else {
					for($i = 0; $i < (sizeof($PILOT_ID)-1); ++$i)
					{
						$sql .= "((kb3_inv_detail.ind_plt_id = " . $PILOT_ID[$i] . ") AND (kb3_kills.kll_victim_id != " . $PILOT_ID[$i] . ")) OR";
					}
					$sql .= "((kb3_inv_detail.ind_plt_id = " . $PILOT_ID[$i] . ") AND (kb3_kills.kll_victim_id != " . $PILOT_ID[$i] . "))";
				}
			}
			$sql .= ") AND (kll_timestamp >= '$startDay 00:00:00') AND (kll_timestamp <= '$endDay 23:59:59')";
			}
			$qry->execute($sql) or die($qry->getErrorMsg());

    $row   =$qry->getRow();

    $cnt   =$row['cnt'];
    $valore=round( $row['valore'] / 1000000, 2 );
}

function TrovaPublic($startDay, $endDay, &$cnt, &$valore, $dtl = 0)
    {
		$qry  =new DBQuery();
		$sql = "
				SELECT 
					COUNT(kll_id) AS cnt, SUM(kll_isk_loss) AS valore
				FROM 
					kb3_kills 
				WHERE 
					(kll_timestamp >= '$startDay 00:00:00') AND
					(kll_timestamp <= '$endDay 23:59:59')";
		$qry->execute($sql)
			or die($qry->getErrorMsg());
		$row   =$qry->getRow();

		$cnt   =$row['cnt'];
		$valore=round( $row['valore'] / 1000000, 2 );
    }
	
function VisualizzaGrafico($arrData)
    {
    //Initialize <chart> element
    if (config::get('fusioncharts_style') == '2D') {
        $style='mscombidy2d';
        $strXML="<chart palette='2' overlapColumns='1' showLegend='"; 
        $strXML.= (config::get('fusioncharts_show_legend')) ? '1' : '0'; 
        $strXML.= "' legendborderalpha='0' legendbgalpha='0' legendshadow='0' showShadow='0' bgColor='000000' bgAlpha='90' canvasBaseColor='333333' canvasBgAlpha='5' canvasborderalpha='0' PYAxisName='Quantity&#10;(Kills/Losses)' pYAxisNameFontColor='CCCCCC' SYAxisName='ISK Value&#10;(Kills/Losses)' sYAxisNameFontColor='CCCCCC' showValues='0' showHoverEffect='1' decimalSeparator='.' thousandSeparator=',' showBorder='0' showLabels='1' labelDisplay='NONE' toolTipBgColor='333333' toolTipColor='CCCCCC' toolTipBorderColor='CCCCCC' anchorBgColor='AAAAAA' baseFontColor='CCCCCC' useplotgradientcolor='0' showplotborder='0' showalternatehgridcolor='0' showalternatevgridcolor='0' showPlotBorder='0' yAxisNamePadding='10'>";
    } else {
        $style='mscolumn3dlinedy';
        $strXML="<chart palette='2' overlapColumns='1' showLegend='";
        $strXML.= (config::get('fusioncharts_show_legend')) ? '1' : '0';
        $strXML.= "' legendborderalpha='0' legendbgalpha='0' legendshadow='0' showShadow='0' bgColor='000000' bgAlpha='90' use3DLighting='1' canvasBgAlpha='10' canvasBgDepth='2' canvasBaseDepth='3' canvasBaseColor='333333' PYAxisName='Quantity&#10;(Kills/Losses)' pYAxisNameFontColor='CCCCCC' SYAxisName='ISK Value&#10;(Kills/Losses)' sYAxisNameFontColor='CCCCCC' showValues='0' showHoverEffect='1' decimalSeparator='.' thousandSeparator=',' showBorder='0' showLabels='1' labelDisplay='NONE' toolTipBgColor='333333' toolTipColor='CCCCCC' toolTipBorderColor='CCCCCC' anchorBgColor='AAAAAA' baseFontColor='CCCCCC' yAxisNamePadding='10'>";
    }
    //Initialize <categories> element - necessary to generate a multi-series chart
    $strCategories="<categories fontColor='CCCCCC'>";

    //Initiate <dataset> elements
    $killcolor = config::get('fusioncharts_kill_color');
    if($killcolor == "") {
        $killcolor = "00AA00";
    }
    $losscolor = config::get('fusioncharts_loss_color');
    if($losscolor == "") {
        $losscolor = "F90000";
    }
    $strDataKills="<dataset seriesName='Kills' color='".$killcolor."'>";
    $strDataLoss="<dataset seriesName='Losses' color='".$losscolor."'>";
    $strDataIskK="<dataset seriesName='Isk Destroyed' alpha='60' color='".$killcolor."' parentYAxis='S' >";
    $strDataIskL="<dataset seriesName='Isk Lost' alpha='60' color='".$losscolor."' parentYAxis='S'>";

    //Iterate through the data    
    foreach ($arrData as $arSubData)
        {
        //Append <category name='...' /> to strCategories
        $strCategories.="<category label='" . $arSubData[1] . "' />";
        //Add <set value='...' /> to both the datasets
        $strDataKills.="<set value='" . $arSubData[2] . "' tooltext=' Kills: " . $arSubData[2] . "'/>";
        $strDataLoss.="<set value='" . $arSubData[3] . "' tooltext=' Losses: " . $arSubData[3] . "'/>";
        $strDataIskK.="<set value='" . $arSubData[4] . "' tooltext=' ISK destroyed: " . $arSubData[4] . " M'/>";
        $strDataIskL.="<set value='" . $arSubData[5] . "' tooltext=' ISK lost: " . $arSubData[5] . " M'/>";
        }

    //Close <categories> element
    $strCategories.="</categories>";

    //Close <dataset> elements
    $strDataKills.="</dataset>";
    $strDataLoss.="</dataset>";
    $strDataIskK.="</dataset>";
    $strDataIskL.="</dataset>";

    //Assemble the entire XML now
    $strXML.=$strCategories . $strDataIskK . $strDataIskL . $strDataKills . $strDataLoss . "</chart>";

    //Create the chart - MS Column 3D Line Combination Chart with data contained in strXML
	//echo getcwd() . "\n";
    $html='<script type="text/javascript" src="'.config::get("cfg_kbhost").'/mods/fusioncharts/charts/fusioncharts.js"></script>';
    $html.="<script type='text/javascript'>FusionCharts.ready(function(){var fusioncharts = new FusionCharts({
type: '".$style."',
theme: 'carbon',
renderAt: 'chart-container',
width: '100%',height: '";
    $html.= (config::get('fusioncharts_show_legend')) ? '180' : '150';
    $html.="',
dataFormat: 'xml',
dataSource:\"".$strXML."\" }); fusioncharts.render(); });</script>";
    if (Config::get('fusioncharts_show_header')) {
    $html.="<div id='kb-fusionchart' style='background-image: url(\"".config::get("cfg_kbhost")."/img/kills.png\");background-repeat: no-repeat;background-position: 0px -3px;margin-top: 10px;margin-bottom: 10px;color: #cccccc;font-size: 12px;font-weight: bold;padding-left: 25px;padding-bottom: 5px;border-bottom: 1px solid #777777;'>Kill Chart</div>";
    }
    $html.="<div id='chart-container'></div>";
    return $html;
    }

/**
 * @return MySQL Date (string)
 * @param integer year (four digits) 
 * @param integer week of the year (1..53)
 * @param integer day of the week (1..7)
 * @desc This function retrieves the time for the given year, week of year and day of the week and returns it.
 */
function mktimefromcw($year, $woy = 1, $dow = 1)
    {
    $dow           =($dow) % 7;
    $woy           =($woy)-1;

    # Get reference value (this is the first monday of the first week of the year, not easy to calculate)
    $fdoy_timestamp=mktime(0, 0, 0, 1, 1, $year);
    $fdoy          =((date("w", $fdoy_timestamp) + 6) % 7) + 1;

    if ($fdoy == 1)
        {
        # This first day of the year is a monday
        $fcwstart=$fdoy_timestamp;
        }
    elseif ($fdoy < 5)
        {
        # The first day if before Friday, therefor the first Monday can be found in the previos year (this is no fun, believe in it!).
        $fcwstart=strtotime("last Monday", $fdoy_timestamp);
        }
    else
        {
        # The first day is a friday or later, so the first days belong to calender week 53 (yes, this is possible!) of the previous year, do not count them for this year.
        $fcwstart=strtotime("next Monday", $fdoy_timestamp);
        }

    # Create timestamp
    $timestr=date("d F Y", $fcwstart) . " +$woy week +$dow day";
    //$timestr = date("Y-m-d H:i:s", $fcwstart)." +$woy week +$dow day";
    $time   =strtotime($timestr);
    //$time = $timestr;

    # Return timestamp
    return date("Y-m-d", $time);
    }
?>
