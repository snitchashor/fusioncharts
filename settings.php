<?php
require_once("common/admin/admin_menu.php");
$page = new Page('Fusioncharts - Settings');

if (isset($_POST['save'])) {
  config::set('fusioncharts_front_page', $_POST["front_page"]);
  config::set('fusioncharts_alliance_page', $_POST["alliance_page"]);
  config::set('fusioncharts_corp_page', $_POST["corp_page"]);
  config::set('fusioncharts_pilot_page', $_POST["pilot_page"]);
  config::set('fusioncharts_kill_color', $_POST["kill_color"]);
  config::set('fusioncharts_loss_color', $_POST["loss_color"]);
  config::set('fusioncharts_style', $_POST["style"]);
  config::set('fusioncharts_show_header', $_POST["show_header"]);
  config::set('fusioncharts_show_legend', $_POST["show_legend"]); 
  $msg .= "Settings updated!";
}

if ($msg) {
	$html .= "<span style='color:green'>".$msg."</span><br />";
}

$killcolor = config::get('fusioncharts_kill_color');
if($killcolor == "") {
        $killcolor = "00AA00";
}
$losscolor = config::get('fusioncharts_loss_color');
if($losscolor == "") {
        $losscolor = "F90000";
}
$style = config::get('fusioncharts_style');
if($style == "") {
        $style = "3D";
}

$html .= "<div class='block-header2'>Fusioncharts locations:</div>";
$html .= "<form method=post action=>";
$html .= "<input type='checkbox' id='front_page' name='front_page'";
$html .= (Config::get('fusioncharts_front_page') ? " checked='checked'": "")." />";
$html .= " Front Page.<br />";
$html .= "<input type='checkbox' id='alliance_page' name='alliance_page'";
$html .= (Config::get('fusioncharts_alliance_page') ? " checked='checked'": "")." />";
$html .= " Alliance Page.<br />";
$html .= "<input type='checkbox' id='corp_page' name='corp_page'";
$html .= (Config::get('fusioncharts_corp_page') ? " checked='checked'": "")." />";
$html .= " Corp Page.<br />";
$html .= "<input type='checkbox' id='pilot_page' name='pilot_page'";
$html .= (Config::get('fusioncharts_pilot_page') ? " checked='checked'": "")." />";
$html .= " Pilot Page.<br />";
$html .= "<div class='block-header2'>Other options:</div>";
$html .= "Color for Kills: #<input type='text' name='kill_color' value='".$killcolor."' /><br />";
$html .= "Color for Losses: #<input type='text' name='loss_color' value='".$losscolor."' /><br />";
$html .= "Graph display style: <select name='style'>";
$html .= "<option value='3D'". ($style == "3D" ? " selected" : "") .">3D</option>";
$html .= "<option value='2D'". ($style == "2D" ? " selected" : "") .">2D</option></select><br />";
$html .= "<input type='checkbox' id='show_header' name='show_header'";
$html .= (Config::get('fusioncharts_show_header') ? " checked='checked'": "")." />";
$html .= " Show kill chart header.<br />";
$html .= "<input type='checkbox' id='show_legend' name='show_legend'";
$html .= (Config::get('fusioncharts_show_legend') ? " checked='checked'": "")." />";
$html .= " Show kill chart legend.<br />";
$html .= "<br /><input type='submit' name='save' value='Save' />";
$html .= "</form>";

$page->setContent($html);
$page->addContext($menubox->generate());
$page->generate();
?>
