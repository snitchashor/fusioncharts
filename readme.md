Fusioncharts mod for EDK 4.2
===
Version 4.2 by Snitch Ashor. Original version by Xeltor.

v4.2 (16/09/07)
 - Updated to latest Fusioncharts.js
 - Removed php-wrapper
 - Option to select colors for kills and losses
 - Option to switch from 3D bargraphs to 2D bargraphs
 - Option to show a header line above the actual graph
 - Fixed number of days when browsing back on a monthly Killboard
